import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import {app} from './api/main';

admin.initializeApp();

export const api = functions.runWith({memory: "512MB"}).region('europe-west1').https.onRequest(app);

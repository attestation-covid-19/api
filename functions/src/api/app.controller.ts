import {Controller, Get} from '@nestjs/common';
import {AppService} from './app.service';
import {AppInfo} from "./app.interface";

@Controller()
export class AppController {

  constructor(private readonly appService: AppService) {}

  @Get()
  getInfo(): AppInfo {
    return this.appService.getInfo();
  }
}

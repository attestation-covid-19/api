import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import * as express from 'express';
import * as helmet from 'helmet';
import * as rateLimit from 'express-rate-limit';
import {ExpressAdapter} from '@nestjs/platform-express';
import {ValidationPipe} from '@nestjs/common';

export const app = express();

const createNestServer = async (expressServer: express.Express): Promise<any> => {
  const nestApplication = await NestFactory.create(AppModule, new ExpressAdapter(expressServer), {cors: true});
  nestApplication.use(helmet());
  nestApplication.use(rateLimit({windowMs: 15 * 60 * 1000, max: 100}));
  nestApplication.useGlobalPipes(new ValidationPipe());

  return nestApplication.init();
};

createNestServer(app)
  .then(() => console.log('Nest server ready!'))
  .catch(error => console.error('Failed to start Nest server', error));

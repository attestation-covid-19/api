import {Body, Controller, Post, Res} from '@nestjs/common';
import {CertificateService} from './certificate.service';
import {CertificateCreateDto} from './models/certificate-create-dto';
import {Response} from 'express';

@Controller('certificate')
export class CertificateController {
  private readonly certificateFileName: string = 'Attestation_de_deplacement_derogatoire.pdf';

  constructor(private readonly certificateService: CertificateService) {}

  @Post('generate')
  async generateCertificate(
    @Body() certificateCreateDto: CertificateCreateDto,
    @Res() response: Response,
  ): Promise<Response<Buffer>> {
    const generatedPdf: Buffer = await this.certificateService.generatePdf(certificateCreateDto, true);
    response.setHeader('Content-Disposition', `attachment; filename=${this.certificateFileName}`);
    response.contentType('application/pdf');

    return response.send(generatedPdf);
  }
}

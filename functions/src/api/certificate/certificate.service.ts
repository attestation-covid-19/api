import {Injectable} from '@nestjs/common';
import {uuid} from 'uuidv4';
import * as admin from 'firebase-admin';
import * as fs from 'fs';
import {execFile} from 'child_process';
import * as moment from 'moment';
import {Bucket} from '@google-cloud/storage';
import * as os from 'os';
import {PDFDocument} from 'pdf-lib';
import {MergedPdf} from './models/merged-pdf.interface';
import {CertificateCreateDto, Purpose} from './models/certificate-create-dto';
import * as fdf from 'utf8-fdf-generator';

@Injectable()
export class CertificateService {
  private readonly templateStoragePath = 'Attestation_de_deplacement_derogatoire.pdf';
  private readonly templateDestinationName = 'Attestation_de_deplacement_derogatoire.pdf';
  private readonly templateDestinationPath = `${os.tmpdir()}/${this.templateDestinationName}`;

  private downloadFileFromStorage = async (filePath: string, destination: string, override = false) => {
    if (!override && fs.existsSync(destination)) {
      return;
    }

    const bucket: Bucket = admin.storage().bucket();
    await bucket.file(filePath).download({destination});
  };

  private mergePdf = (sourcePdf: string, destinationPdf: string, data: { [key: string]: string }, flatten = false, tempPath: string): Promise<MergedPdf> => {
    return new Promise<MergedPdf>((resolve, reject) => {
      this.fillFormWithOptions(sourcePdf, destinationPdf, data, flatten, tempPath, (error: any) => {
        if (error) reject(error);
        resolve({source: sourcePdf, destination: destinationPdf});
      });
    });
  };

  public formatCertificateDto = (certificateDto: CertificateCreateDto): { [key: string]: string } => {
    const formattedBirthDay = moment(certificateDto.birthDate).format('DD/MM/YYYY');
    const formattedCreationDate = moment(certificateDto.creationDate).format('DD/MM/YYYY');
    const formattedCreationHour = certificateDto.creationTime.split(':')[0];
    const formattedCreationMinute = certificateDto.creationTime.split(':')[1];

    return {
      'Nom et prénom': certificateDto.name,
      'Date de naissance': formattedBirthDay,
      'Lieu de naissance': certificateDto.birthCity,
      'Adresse actuelle': `${certificateDto.address} ${certificateDto.zipCode} ${certificateDto.city}`,
      'Déplacements entre domicile et travail': certificateDto.reason === Purpose.work ? 'Oui' : 'Off',
      'Déplacements achats nécéssaires': certificateDto.reason === Purpose.shopping ? 'Oui' : 'Off',
      'Consultations et soins': certificateDto.reason === Purpose.health ? 'Oui' : 'Off',
      'Déplacements pour motif familial': certificateDto.reason === Purpose.social ? 'Oui' : 'Off',
      'Déplacements brefs (activité physique et animaux)': certificateDto.reason === Purpose.sport ? 'Oui' : 'Off',
      'Convcation judiciaire ou administrative': certificateDto.reason === Purpose.convocation ? 'Oui' : 'Off',
      'Mission d\'intérêt général': certificateDto.reason === Purpose.missions ? 'Oui' : 'Off',
      'Ville': certificateDto.creationLocation,
      'Date': formattedCreationDate,
      'Heure': formattedCreationHour,
      'Minute': formattedCreationMinute,
    }
  };

  private fillFormWithOptions = (sourceFile: string, destinationFile: string, fieldValues: { [key: string]: string }, shouldFlatten: boolean, tempFDFPath: string, callback: (error?: any) => any) => {
    const randomSequence = Math.random().toString(36).substring(7);
    const currentTime = new Date().getTime();
    const tempFDFFile = `temp_data${currentTime + randomSequence}.fdf`;
    const tempFDF = tempFDFPath !== undefined ? `${tempFDFPath}/${tempFDFFile}` : tempFDFFile;
    fdf.generator(fieldValues, tempFDF);
    const args = [sourceFile, 'fill_form', tempFDF, 'output', destinationFile];
    const pdfFillerBin = `${__dirname}/../../../node_modules/pdffiller-aws-lambda/bin`;

    process.env.PATH = `${process.env.PATH}:${pdfFillerBin}`;
    process.env.LD_LIBRARY_PATH = pdfFillerBin;

    if (shouldFlatten) {
      args.push('flatten');
    }

    execFile('pdftk', args, (error) => {
      if (error) return callback(error);

      fs.unlink(tempFDF, function (err) {
        if (err) return callback(err);
        return callback();
      });
    });
  };

  private addSignatureImageToPdf = async (sourcePdfPath: string, signatureImage: string, x: number, y: number): Promise<Uint8Array> => {
    const uint8Array = fs.readFileSync(sourcePdfPath);
    const pdfDoc = await PDFDocument.load(uint8Array);
    const page = pdfDoc.getPages()[0];
    const signatureImg = await pdfDoc.embedPng(signatureImage);
    const signatureDim = signatureImg.scale(1 / (signatureImg.width / 85));

    page.drawImage(signatureImg, { x, y, width: signatureDim.width, height: signatureDim.height });
    return await pdfDoc.save();
  };

  private deleteFile = (filePath: string): Promise<any> => {
    return new Promise((resolve, reject) => {
      fs.unlink(filePath, (error: any) => {
        if (error) reject(error);
        resolve(error);
      });
    });
  };

  public generatePdf = async (certificateCreateDto: CertificateCreateDto, deleteFileAfter = true): Promise<Buffer> => {
    await this.downloadFileFromStorage(this.templateStoragePath, this.templateDestinationPath);
    const formattedData = this.formatCertificateDto(certificateCreateDto);
    const destinationPdf = `${os.tmpdir()}/${uuid()}_${Date.now()}.pdf`;
    const mergedPdf: MergedPdf = await this.mergePdf(this.templateDestinationPath, destinationPdf, formattedData, true, os.tmpdir());
    const signedCertificateFile = await this.addSignatureImageToPdf(mergedPdf.destination, certificateCreateDto.signature, 170, 130);
    const buffer = Buffer.from(signedCertificateFile);

    if (deleteFileAfter) {
      await this.deleteFile(mergedPdf.destination);
    }

    return buffer;
  }
}

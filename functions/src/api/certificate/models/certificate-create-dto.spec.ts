import { CertificateCreateDto } from './certificate-create-dto';

describe('CertificateCreateDto', () => {
  it('should be defined', () => {
    expect(new CertificateCreateDto()).toBeDefined();
  });
});

export interface MergedPdf {
  source: string;
  destination: string;
}

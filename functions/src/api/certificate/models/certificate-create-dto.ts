import {IsDateString, IsEnum, IsNotEmpty, IsOptional, IsString, Length} from 'class-validator';

export enum Purpose {
  work = 'work',
  shopping = 'shopping',
  health = 'health',
  social = 'social',
  sport = 'sport',
  convocation = 'convocation',
  missions = 'missions',
}
export class CertificateCreateDto {
  @IsNotEmpty()
  @IsString()
  name: string;

  @IsNotEmpty()
  @IsDateString()
  birthDate: string;

  @IsNotEmpty()
  @IsString()
  birthCity: string;

  @IsNotEmpty()
  @IsString()
  address: string;

  @IsNotEmpty()
  @IsString()
  city: string;

  @IsNotEmpty()
  @IsString()
  zipCode: string;

  @IsNotEmpty()
  @IsEnum(Purpose)
  reason: Purpose;

  @IsNotEmpty()
  @IsString()
  creationLocation: string;

  @IsNotEmpty()
  @IsDateString()
  creationDate: string;

  @IsNotEmpty()
  @IsString()
  @Length(5, 5)
  creationTime: string;

  @IsString()
  @IsOptional()
  signature: string;
}

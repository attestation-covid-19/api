import {Injectable} from '@nestjs/common';
import {AppInfo} from "./app.interface";

@Injectable()
export class AppService {

  getInfo(): AppInfo {
    return { version: '1.3.5' };
  }
}
